from functions import m_json
from functions import m_pck

#Versuch heat_capacity 

setup_path = "datasheets/setup_heat_capacity.json"                                                         #Pfad der heat_capacity json Datei
folder_path = "datasheets"
path_data_folder = 'data/heat_capacity'                                                                    #Pfad der Ablage der Versuchsdateien


metadata = m_json.get_metadata_from_setup(setup_path)                                                      #Metadaten von Setup heat_capacity auslesen
m_json.add_temperature_sensor_serials(folder_path, metadata)                                               #Zu den Metadaten beide Seriennummern der Sensoren vereinen
gemessene_Daten = m_pck.get_meas_data_calorimetry(metadata)
_pck.logging_calorimetry(gemessene_Daten, metadata, path_data_folder, folder_path)
m_json.archiv_json(folder_path, setup_path, path_data_folder)                                              #Archiv aller json Datien die zum Versuch gehören im Ordner "heat_capacity" ablegen




##Versuch newton

#setup_path = "datasheets/setup_newton.json"                                                               #Pfad der newton json Datei
#folder_path = "datasheets"
#path_data_folder = 'data/newton'                                                                          #Pfad der Ablage der Versuchsdateien


#metadata = m_json.get_metadata_from_setup(setup_path)                                                     #Metadaten von Setup newton auslesen
#m_json.add_temperature_sensor_serials(folder_path, metadata)                                              #Zu den Metadaten beide Seriennummern der Sensoren vereinen
#gemessene_Daten = m_pck.get_meas_data_calorimetry(metadata)
#m_pck.logging_calorimetry(gemessene_Daten, metadata, path_data_folder, folder_path)
#m_json.archiv_json(folder_path, setup_path, path_data_folder)                                             #Archiv aller json Datien die zum Versuch gehören im Ordner "newton" ablegen



#je nachdem welcher Versuch gestartet wird, wird jeweils der andere Teil auskommentiert, diese Darstellung habe ich gewählt um den Ablauf ersichtlich zu machen